import os
import uuid
import logging

from tempfile import mkdtemp
import json

from monseigneur.core.backend import BackendManager


class Fetcher(BackendManager):

    def __init__(self, *args, **kwargs):
        self.data_path = os.path.join(os.environ['HOME'], "mdev/monseigneur")
        self.config_path = os.path.join(os.environ['HOME'], "mdev/monseigneur/mbackend/")
        self.home_path = os.environ['HOME']

        is_public = kwargs.pop("is_public", None)
        if is_public:
            status = "public"
        else:
            status = "private"
        custom_path = kwargs.pop("custom_path", None)
        if custom_path:
            self.PATH =  os.path.join(self.home_path, custom_path)
        else:
            self.PATH = os.path.join(self.data_path, "monseigneur/modules/{}".format(status))

        absolute_path = kwargs.pop("absolute_path", None)
        if absolute_path:
            assert absolute_path and not any([is_public, custom_path])
            self.PATH = os.path.join(self.home_path, absolute_path)

        super(Fetcher, self).__init__(self.PATH, *args, **kwargs)
        logging.basicConfig()
        self.load_config()

        self.DATA_DIR = os.path.join(self.data_path, self.saved_responses_path)

    def load_config(self):
        with open(os.path.join(self.config_path, 'config.json')) as json_data_file:
            self.config = json.load(json_data_file)
        self.saved_responses_path = os.path.join(os.environ['HOME'], self.config["saved_responses_config"]["responses_path"][0])

    def build_backend(self, module_name, params):
        backend_prefix = 'conn_%s_' % module_name
        debug_dir_path = self.DATA_DIR
        self.create_logs_folders(debug_dir_path)
        debug_dir = mkdtemp(prefix=backend_prefix, dir=debug_dir_path) if debug_dir_path else None
        backend_name = os.path.basename(debug_dir) if debug_dir else backend_prefix + uuid.uuid4().hex

        params['_debug_dir'] = debug_dir or ''
        storage = None

        backend = super(Fetcher, self).build_backend(module_name, params, storage, name=backend_name)
        backend.name = backend_name
        backend.debug_dir = debug_dir
        return backend

    def create_logs_folders(self, debug_dir_path):
        if self.DATA_DIR:
            if not os.path.exists(debug_dir_path):
                os.makedirs(debug_dir_path)
