# -*- coding: utf-8 -*-

from __future__ import absolute_import

import sys
import os
import logging
import locale
import json
import cgitb
from pytz import timezone
from logging.handlers import RotatingFileHandler
from mbackend.tools.email_tools.backtrace_sender import backtrace_sender

from monseigneur.core.tools.log import createColoredFormatter, getLogger

__all__ = ['Application']


class Application(object):

    try:
        data_path = os.environ['DATAPATH']
    except KeyError:
        data_path = '{}/{}'.format(os.environ['HOME'], 'mdev/monseigneur/mbackend')

    LOG_LEVEL = logging.DEBUG

    def __init__(self, name, send_email=False, tz='Europe/Paris'):
        self.name = name
        self.send_email = send_email
        self.logger = getLogger(name)
        self.load_config()
        self.BACKTRACE_FILENAME = os.path.join(os.environ['HOME'], "{}.log".format(self.name))
        self.create_backtrace_logger()
        self.tz = timezone(tz)

        try:
            locale.setlocale(locale.LC_ALL, '')
        except locale.Error:
            pass

        self.my_backtrace_sender = backtrace_sender(config=self.config)
        # sys.excepthook = self.exception_handler

    def exception_handler(self, exc_type, exc_value, exc_tb):

        if self.send_email is True:
            trace = cgitb.text((exc_type, exc_value, exc_tb))
            self.my_backtrace_sender.send_backtrace(backtrace="Uncaught exception: {0}".format(str(trace)), appname=self.APPNAME, owner=self.OWNER)

        self.backtrace_logger.error("Logging exception", exc_info=(exc_type, exc_value, exc_tb))

    def load_config(self):

        with open(os.path.join(self.data_path, 'config.json')) as json_data_file:
            self.config = json.load(json_data_file)
        self.log_path = os.path.join(self.data_path, self.config["log_config"]["log_path"][0])

    def setup_logging(self):
        logging.root.handlers = []
        logging.root.setLevel(self.LOG_LEVEL)
        logging.root.addHandler(self.create_default_logger())

    def create_file_logger(self, quiet=False, rotation=True):
        path = self.log_path
        if not os.path.exists(path):
            try:
                os.makedirs(path)
            except OSError as e:
                self.logger.error('Unable to create logging directory %s: %s', path, e)
                if quiet:
                    return None
                sys.exit(1)

        filename = os.path.join(path, self.LOG_FILENAME)

        try:
            handler = RotatingFileHandler(filename, maxBytes=10*1024*1024, backupCount=5)
        except IOError as e:
            self.logger.error('Unable to create the logging file %s: %s', filename, e)
            if quiet:
                return None
            sys.exit(1)
        else:
            format = '%(asctime)s:%(levelname)s:%(name)s:%(filename)s:%(lineno)d:%(funcName)s %(message)s'
            handler.setFormatter(logging.Formatter(format))
            logging.root.addHandler(handler)
            return handler

    def create_backtrace_logger(self):
        self.backtrace_logger = logging.getLogger('backtrace_logger')
        path = self.log_path
        filename = os.path.join(path, self.BACKTRACE_FILENAME)
        handler = RotatingFileHandler(filename, maxBytes=10*1024*1024, backupCount=5)
        format = '%(asctime)s:%(levelname)s:%(name)s:%(filename)s:%(lineno)d:%(funcName)s %(message)s'
        handler.setFormatter(logging.Formatter(format))
        self.backtrace_logger.addHandler(handler)
        return handler

    # creates the logger for the console
    @classmethod
    def create_default_logger(klass):
        format = '%(asctime)s:%(levelname)s:%(name)s:%(filename)s:%(lineno)d:%(funcName)s %(message)s'
        handler = logging.StreamHandler(sys.stdout)
        handler.setFormatter(createColoredFormatter(sys.stdout, format))
        return handler
