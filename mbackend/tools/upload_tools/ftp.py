#!/usr/bin/python
import ftplib


def ftp_send_file(remote_address, username, password, filepath, destination_filename):
    session = ftplib.FTP(remote_address, username, password)
    file = open(filepath, 'rb')
    session.storbinary('STOR '+ destination_filename, file)     # send the file
    file.close()                                    # close file and FTP
    session.quit()
