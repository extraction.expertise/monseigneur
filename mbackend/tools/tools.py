from monseigneur.core.browser.exceptions import ServerError, ClientError

import time
import traceback
from itertools import cycle, islice
from socket import gaierror
from random import shuffle, randint
import bugsnag
import socket
import pickle
import os
from ssl import SSLError
from requests.exceptions import ProxyError, ChunkedEncodingError, ConnectTimeout, ReadTimeout, ConnectionError, ContentDecodingError

from urllib3.exceptions import ReadTimeoutError
from socket import timeout
from shadow_useragent import ShadowUserAgent
from mbackend.tools.proxies import proxytools


class TooManyProxyErrors(Exception):
    pass


class BackendTools:

    def __init__(self):
        self.proxytools = proxytools()

    def kill_worker(self, command=None):
        if not command:
            os.system("pkill -9 -f '{} worker'".format(self.module_name))
        else:
            print(command)
            os.system(command)

    def execute_request(self, function, *args, **kwargs):

        if not hasattr(self, 'max_ban'):
            self.max_ban = 3

        if not hasattr(self, 'max_502'):
            self.max_502 = 5

        if not hasattr(self, 'max_proxy_errors'):
            self.max_proxy_errors = 30

        retry_count = 0
        server_error_502 = 0
        no_root_count = 0
        proxy_auth_count = 0
        ban_count = 0

        if not hasattr(self, 'proxy_request_count'):
            self.proxy_request_count = 0
        self.proxy_request_count += 1

        while 1:
            try:
                return function(*args, **kwargs)
            except (ConnectionError, ProxyError, ConnectTimeout, ReadTimeout, ChunkedEncodingError, SSLError, ReadTimeoutError, timeout, ContentDecodingError) as e:
                retry_count += 1
                self.logger.error(traceback.format_exc())
                if retry_count > self.max_proxy_errors:
                    raise TooManyProxyErrors(e)
            except AssertionError as e:
                if str(e) == "ElementTree not initialized, missing root":
                    no_root_count += 1
                    time.sleep(10)
                    if no_root_count > 8:
                        raise e
                else:
                    raise(e)
            except ServerError as e:
                if e.response.status_code in (502, 504):
                    server_error_502 += 1
                    if server_error_502 > self.max_502:
                        raise(e)
                    time.sleep(randint(5, 10))
                elif e.response.status_code in (503, 500):
                    ban_count += 1
                    time.sleep(randint(5, 10))
                    if ban_count > self.max_ban:
                        raise(e)
                else:
                    raise(e)
            except ClientError as e:
                if e.response.status_code == 407:
                    proxy_auth_count += 1
                    if proxy_auth_count > 3:
                        raise Exception(e)
                else:
                    raise(e)
            finally:
                pass

    def load_states_mixin(self):
        try:
            pickle_in = open(str(self.states_mixin_path + "/state_{}.pickle".format(self.ip_index)), 'rb')
            print(str(self.states_mixin_path + "/state_{}.pickle".format(self.ip_index)))
        except FileNotFoundError:
            self.logger.warning("no state registered")
        else:
            state = pickle.load(pickle_in)
            self.module.browser.load_state(state)

    def save_states_mixin(self):
        assert self.states_mixin_path
        print(self.states_mixin_path)
        assert self.ip_index is not None
        state = self.module.browser.dump_state()
        pickle_out = open(str(self.states_mixin_path + "/state_{}.pickle".format(self.ip_index)), 'wb')
        pickle.dump(state, pickle_out)
        pickle_out.close()
        self.logger.warning("state dumped successfull")

    def bugsnag_notify(self, context, exception, other=None, extra_other=None):
        bugsnag.notify(
            Exception(exception),
            context=context,
            meta_data={"special_info": {"browser": self.module.browser.__dict__, "other": other, "extra_other": extra_other}}
        )

    def assign_single_luminati_proxy(self, raw_proxy):

        gaierr = 0
        while 1:
            try:
                super_proxy = socket.gethostbyname('zproxy.lum-superproxy.io')
            except gaierror:
                time.sleep(3)
                gaierr += 1
                if gaierr > 10:
                    raise Exception("too much gaierror")
            else:
                break
        value_list = [value.strip() for value in raw_proxy.split(":")]
        self.luminati_ip = value_list[2] + "-country-fr-dns-remote"
        formated_proxy = "{}:{}@{}:{}".format(self.luminati_ip, value_list[3], super_proxy, value_list[1])
        self.proxy_dictionaries = self.proxytools.forge_proxy_dictionaries([formated_proxy])
        self.logger.warning(self.proxy_dictionaries[0])
        self.module.browser.PROXIES = self.proxy_dictionaries[0]
        self.logger.error(self.proxy_dictionaries[0])

    def assign_luminati_proxy(self):
        counter = 0

        assert self.ip_path
        assert self.ip_index is not None
        assert self.proxytools

        with open(self.ip_path) as f:
            for _, _ in enumerate(f):
                counter += 1
            self.ip_list_len = counter

        if self.ip_index > self.ip_list_len:
            missing_ips = self.ip_index - self.ip_list_len
            raise Exception(f"IPs < Threads. Please, add {missing_ips} IPs")

        gaierror_count = 0

        while True:

            try:
                super_proxy = socket.gethostbyname('zproxy.lum-superproxy.io')
            except gaierror:
                time.sleep(3)
                gaierror_count += 1
                if gaierr > 10:
                    raise Exception("TooMany gaierrors")
            else:
                break

        with open(self.ip_path) as f:
            for index, value in enumerate(f):
                if index == self.ip_index:
                    value_list = [value.strip() for value in value.split(":")]
                    self.luminati_ip = value_list[2] + "-country-fr-dns-remote"
                    formated_proxy = "{}:{}@{}:{}".format(self.luminati_ip, value_list[3], super_proxy, value_list[1])
                    self.proxy_dictionaries = self.proxytools.forge_proxy_dictionaries([formated_proxy])
                    self.logger.warning(self.proxy_dictionaries[0])
                    self.module.browser.PROXIES = self.proxy_dictionaries[0]
                    self.logger.error(self.proxy_dictionaries[0])
                    return

    def assign_proxy(self):
        # path for your ips to assign
        assert self.ip_path
        # index of the ip to assign in the list
        assert self.ip_index
        assert self.proxytools
        assert self.proxy_dictionaries
        with open(self.ip_path) as f:
            for index, proxy in enumerate(f):
                if index == self.ip_index:
                    self.proxy_dictionaries = self.proxytools.forge_proxy_dictionaries([proxy])
                    self.logger.warning(self.proxy_dictionaries[0])
                    self.module.browser.PROXIES = self.proxy_dictionaries[0]
                    self.logger.error(self.proxy_dictionaries[0])
                    break

    def assign_single_proxy(self, proxy_adress):
        # path for your ips to assign
        # index of the ip to assign in the list
        assert self.proxytools
        self.proxy_dictionaries = self.proxytools.forge_proxy_dictionaries([proxy_adress])
        self.logger.warning(self.proxy_dictionaries[0])
        self.module.browser.PROXIES = self.proxy_dictionaries[0]

    def set_luminati_proxy(self):
        with open(self.ip_path) as f:
            for index, proxy in enumerate(f):
                self.proxy_dictionaries = self.proxytools.forge_proxy_dictionaries([proxy])
                self.logger.warning(self.proxy_dictionaries[0])
                self.module.browser.PROXIES = self.proxy_dictionaries[0]
                self.logger.error(self.proxy_dictionaries[0])
                break


def ip_pool_retry(exceptions_to_check, pool_size=100, _shuffle=True, cookies_clear=True, switch_ua=True, no_mobile=True, tries=3, delay=1, backoff=1):
    def deco_retry(f):
        def f_retry(self, *args, **kwargs):

            mtries = kwargs.pop('_tries', tries)
            mdelay = kwargs.pop('_delay', delay)

            try:
                assert self.PROXIES
            except AssertionError:
                self.logger.warning("Need to add `PROXIES`")
                raise
            try:
                assert self.module
            except AssertionError:
                self.logger.warning("Need to add `module`")
                raise

            while mtries > 1:
                try:
                    return f(self, *args, **kwargs)
                except exceptions_to_check as exc:
                    if _shuffle:
                        self.logger.warning('Shuffling PROXIES...')
                        self.PROXIES = list(islice(self.PROXIES, 0, pool_size))
                        shuffle(self.PROXIES)
                        self.PROXIES = cycle(iter(self.PROXIES))
                        pass
                    proxy = next(self.PROXIES)
                    self.logger.warning('Getting next Proxy %s' % proxy['https'])
                    self.module.browser.PROXIES = proxy
                    if cookies_clear:
                        self.logger.warning('Clearing Cookies')
                        self.module.browser.session.cookies.clear()
                    if switch_ua:
                        if no_mobile:
                            new_ua = ShadowUserAgent().random_nomobile
                        else:
                            new_ua = ShadowUserAgent().random
                        self.logger.warning('Switching UA %s' % new_ua)
                        self.module.browser.session.headers['User-Agent'] = new_ua
                    self.logger.error('%s, Retrying in %d seconds...' % (exc, mdelay))
                    time.sleep(mdelay)
                    mtries -= 1
                    mdelay *= backoff
            return f(self, *args, **kwargs)
        return f_retry
    return deco_retry
