from sqlalchemy import Column, Integer, String, ForeignKey
from sqlalchemy.orm import relationship
from sqlalchemy.types import Boolean, DateTime, Date, Float

from mbackend.alchemy.core.base import Base

# Don't forget to clean this comment when you're done!
# class Example(Base):
#     """Base Example object for sqlalchemy-driven db."""
#
#     __tablename__ = "example"
#
#     id = Column(Integer, primary_key=True, autoincrement=True)
#     example_attr = Column(String)


def create_all(engine):
    print("creating databases")
    Base.metadata.create_all(engine)
