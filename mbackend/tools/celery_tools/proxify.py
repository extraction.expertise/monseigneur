import socket


class Proxify:

    def __init__(self):
        self.proxy_dictionaries = None

    def assign_luminati_proxy(self):
        # path for your ips to assign
        print('mmmm')
        self.logger.error(self.ip_path)
        assert self.ip_path is not None
        assert self.proxytools
        gaierror_count = 0
        while 1:
            try:
                super_proxy = socket.gethostbyname('zproxy.lum-superproxy.io')
            except Exception as e:
                print(e)
                gaierror_count += 1
                if gaierror_count > 5:
                    raise
            else:
                break
        with open(self.ip_path) as f:
            for index, value in enumerate(f):
                if index == self.ip_index:
                    value_list = [value.strip() for value in value.split(":")]
                    formated_proxy = "{}:{}@{}:{}".format(value_list[2], value_list[3], super_proxy, value_list[1])
                    proxy_dict = {
                        f"{k}": f"{k}://{formated_proxy}" for k in ['http', 'https', 'ftp']
                    }
                    self.module.browser.PROXIES = proxy_dict
                    self.logger.error(self.module.browser.PROXIES)
                    break

    def assign_proxy(self):
        # path for your ips to assign
        assert self.ip_path
        # index of the ip to assign in the list
        assert self.ip_index
        assert self.proxytools
        with open(self.ip_path) as f:
            for index, proxy in enumerate(f):
                if index == self.ip_index:
                    self.proxy_dictionaries = self.proxytools.forge_proxy_dictionaries([proxy])
                    self.logger.warning(self.proxy_dictionaries[0])
                    self.module.browser.PROXIES = self.proxy_dictionaries[0]
                    self.logger.error(self.proxy_dictionaries[0])
                    break

    def assign_single_proxy(self):
        # path for your ips to assign
        assert self.proxy_adress
        # index of the ip to assign in the list
        assert self.proxytools
        self.proxy_dictionaries = self.proxytools.forge_proxy_dictionaries([self.proxy_adress])
        self.logger.warning(self.proxy_dictionaries[0])
        self.module.browser.PROXIES = self.proxy_dictionaries[0]
