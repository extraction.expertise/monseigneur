from mbackend.alchemy.scoped_dao.dao_factory import DaoFactory
from mbackend.alchemy.modules.watches.christies.tables import Monitoring

from ..tables import create_all

from mbackend.tools.db_tools.decorators import dbconnect, dbconnect_noexpire, dbconnect_noclosing, dbconnect_noclosingandsession, dbconnect_sharedsession
from pytz import timezone

from sqlalchemy.orm import scoped_session
from sqlalchemy import and_, or_


class DaoFactory(DaoFactory):

    def __init__(self, database_name, engine_config=None):
        self.timezone = timezone('Europe/Paris')
        super(DaoFactory, self).__init__(database_name, engine_config)
        create_all(self.engine)

    def get_shared_session(self):
        ScopedSession = scoped_session(self.session_factory)
        session = ScopedSession()
        return session, ScopedSession

    def select_by_id(self, session, alchemy_object, instance_object, id_name):
        assert isinstance(id_name, str)
        assert hasattr(alchemy_object, id_name)
        assert hasattr(instance_object, id_name)
        return session.query(alchemy_object).filter(getattr(alchemy_object, id_name) == getattr(instance_object, id_name)).one()

    def select_monitoring(self, session):
        return session.query(Monitoring).filter(or_(and_(or_(Monitoring.last_auction_scraped < Monitoring.total_auctions,
                                                Monitoring.total_auctions == None, Monitoring.last_auction_scraped == None), Monitoring.total_auctions != 0), Monitoring.total_auctions == None)).all()

    @dbconnect
    def count_by_id(self, session, parent_object, child_object, id_name):
        assert isinstance(id_name, str)
        assert hasattr(parent_object, id_name)
        assert hasattr(child_object, id_name)
        return session.query(parent_object).filter(getattr(parent_object, id_name) == getattr(child_object, id_name)).count()

    def monitoring_exists(self, session, monitoring):
        return session.query(Monitoring).filter(Monitoring.month == monitoring.month, Monitoring.year == monitoring.year).count()
