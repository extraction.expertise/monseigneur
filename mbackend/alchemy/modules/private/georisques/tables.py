import datetime

from sqlalchemy import Column, Integer
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.types import String, DateTime
from sqlalchemy.dialects.mysql import LONGTEXT, VARCHAR, DATETIME, BOOLEAN


class Base(object):
    __table_args__ = {'mysql_charset': 'utf8mb4', 'mysql_collate': 'utf8mb4_bin'}
    __mysql_charset = 'utf8mb4'


Base = declarative_base(Base)


class Task(Base):
    __tablename__ = "tasks"

    id = Column(Integer, primary_key=True, autoincrement=True)
    unique_task_id = Column(VARCHAR(250), unique=True)
    base64_pdf = Column(LONGTEXT)
    addr_string = Column(String(length=500))
    path = Column(VARCHAR(500))
    creation_time = Column(DATETIME)
    scraping_date = Column(DateTime, default=datetime.datetime.utcnow())
    time_elapsed = Column(Integer)
    status = Column(Integer, default=0)
    backtrace = Column(LONGTEXT)


def create_all(engine):
    print("creating databases")
    Base.metadata.create_all(engine)
