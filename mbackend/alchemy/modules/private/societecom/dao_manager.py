from mbackend.alchemy.scoped_dao.dao_manager import DaoManager
from .dao_factory import DaoFactory


class DaoManager(DaoManager):

    def __init__(self, table_name, engine_config=None):
        super(DaoManager, self).__init__()
        self.daoFactory = DaoFactory(table_name, engine_config)

    def get_shared_session(self):
        return self.daoFactory.get_shared_session()

    def select_by_id(self, alchemy_object, id):
        return self.daoFactory.select_by_id(alchemy_object, id)

    # creating function to count if company already in base
    def count_by_siret(self, alchemy_object, company_object):
        return self.daoFactory.count_by_siret(alchemy_object, company_object)

    def count_by_nom_and_ape(self, alchemy_object, nom, ape):
        return self.daoFactory.count_by_nom_and_ape(alchemy_object, nom, ape)
