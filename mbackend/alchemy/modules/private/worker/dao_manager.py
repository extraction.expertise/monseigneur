from mbackend.alchemy.scoped_dao.dao_manager import DaoManager
from .dao_factory import DaoFactory


class DaoManager(DaoManager):

    def __init__(self, table_name, engine_config=None, tables=None):
        super(DaoManager, self).__init__()
        self.dao_factory = DaoFactory(table_name, engine_config)

    def get_last_annonce_scraped(self):
        return self.dao_factory.get_last_annonce_scraped()

    def sum_for_group(self, session, group_slug):
        return self.dao_factory.sum_for_group(session, group_slug)

    def get_annonce_from_user(self, session, annonce_obj):
        return self.dao_factory.get_annonce_from_user(session, annonce_obj)
