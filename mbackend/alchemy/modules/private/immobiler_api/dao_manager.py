
class ImmobilierManager:

    def select_annonce_type(self, session, annonce_type):
        return self.dao_factory.select_annonce_type(session, annonce_type)

    def select_category(self, session, category):
        return self.dao_factory.select_category(session, category)

    def select_agency(self, session, agency):
        return self.dao_factory.select_agency(session, agency)

    def select_picture(self, session, picture):
        return self.dao_factory.select_picture(session, picture)

    def select_heat_type(self, session, heat_type):
        return self.dao_factory.select_heat_type(session, heat_type)

    def select_kitchen_type(self, session, kitchen_type):
        return self.dao_factory.select_kitchen_type(session, kitchen_type)

    def agency_exists(self, session, agency):
        return self.dao_factory.agency_exists(session, agency)

    def annonce_exists(self, session, annonce):
        return self.dao_factory.annonce_exists(session, annonce)

    def select_by_field(self, session, alchemy_object, field):
        return self.dao_factory.select_by_field(session, alchemy_object, id)
