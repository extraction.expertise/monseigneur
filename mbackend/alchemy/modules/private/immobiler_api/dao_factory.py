from monseigneur.modules.private.seloger_immobilier.alchemy.tables import Annonce, Agency, Picture

from sqlalchemy.sql.expression import exists
from sqlalchemy.orm.exc import NoResultFound


class ImmobilierFactory():

    def select_agency(self, session, agency):
        existing_agency = self.select_by_field(session, Agency, agency, "agency_id")
        if existing_agency:
            assert existing_agency.agency_id == agency.agency_id
            return existing_agency
        return agency

    def select_picture(self, session, picture):
        existing_picture = self.select_by_field(session, Picture, picture, "url")
        if existing_picture:
            assert existing_picture.url == picture.url
            return existing_picture
        return picture

    def agency_exists(self, session, agency_id):
        return session.query(exists().where(Agency.agency_id == agency_id)).scalar()

    def annonce_exists(self, session, annonce):
        return session.query(exists().where(Annonce.annonce_id == annonce.annonce_id)).scalar()

    def select_by_field(self, session, parent_object, instance_object, field):
        try:
            return session.query(parent_object).filter(getattr(parent_object, field) == getattr(instance_object, field)).one()
        except NoResultFound:
            return None
