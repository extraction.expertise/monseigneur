from mbackend.alchemy.scoped_dao.dao_manager import DaoManager
from .dao_factory import DaoFactory


class DaoManager(DaoManager):

    def __init__(self, table_name, engine_config=None):
        super(DaoManager, self).__init__()
        self.daoFactory = DaoFactory(table_name, engine_config)

    def get_shared_session(self):
        return self.daoFactory.get_shared_session()

    def select_by_annonce_id(self, session, alchemy_object, id):
        return self.daoFactory.select_by_annonce_id(session, alchemy_object, id)

    def select_monitoring(self):
        return self.daoFactory.select_monitoring()

    def insert_annonce(self, session, annonce):
        return self.daoFactory.insert_annonce(session, annonce)

    def annonce_exists(self, annonce):
        return self.daoFactory.annonce_exists(annonce)
