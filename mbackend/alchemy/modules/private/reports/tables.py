from sqlalchemy import Column, String, Integer, ForeignKey, Table, Float
from sqlalchemy.orm import relationship, backref

from sqlalchemy.dialects.mysql import TINYTEXT, MEDIUMTEXT, LONGTEXT, VARCHAR, TINYINT, DECIMAL
from sqlalchemy.types import DateTime

from mbackend.alchemy.core.base import Base


class ErrorReport(Base):
    __tablename__ = "error_report"

    id = Column(Integer, primary_key=True, autoincrement=True)
    error_date = Column(DateTime)
    ip = Column(Integer)
    backtrace = Column(MEDIUMTEXT)
    browser = Column(MEDIUMTEXT)
    current_url = Column(MEDIUMTEXT)
