from sqlalchemy import Column, String, Integer, ForeignKey, Table
from sqlalchemy.orm import relationship, backref
from sqlalchemy.ext.declarative import declarative_base

from sqlalchemy.dialects.mysql import TINYTEXT, MEDIUMTEXT, LONGTEXT, VARCHAR, TINYINT, FLOAT, MEDIUMINT, JSON, BOOLEAN
from sqlalchemy.types import DateTime


class Base(object):
    __table_args__ = {'mysql_charset': 'utf8mb4', 'mysql_collate': 'utf8mb4_bin'}
    __mysql_charset = 'utf8mb4'


Base = declarative_base(Base)


class Item(Base):
    __tablename__ = 'item'

    id = Column(Integer, primary_key=True, autoincrement=True)
    item_id = Column(VARCHAR(100), unique=True)
    street_address = Column(MEDIUMTEXT)
    cp = Column(Integer)
    activite = Column(MEDIUMTEXT)
    phone = Column(MEDIUMTEXT)
    source_url = Column(MEDIUMTEXT)
    destination_url = Column(MEDIUMTEXT)

    budget = Column(MEDIUMTEXT)
    type_cuisine = Column(MEDIUMTEXT)

    medical_status = Column(MEDIUMTEXT)
    first_name = Column(MEDIUMTEXT)
    last_name = Column(MEDIUMTEXT)

    image = Column(MEDIUMTEXT)
    website = Column(MEDIUMTEXT)
    description = Column(MEDIUMTEXT)
    prestations = Column(MEDIUMTEXT)
    ambiance = Column(MEDIUMTEXT)
    produits = Column(MEDIUMTEXT)
    moyens_paiement = Column(MEDIUMTEXT)
    last_update = Column(MEDIUMTEXT)
    horaire_lundi = Column(MEDIUMTEXT)
    horaire_mardi = Column(MEDIUMTEXT)
    horaire_mercredi = Column(MEDIUMTEXT)
    horaire_jeudi = Column(MEDIUMTEXT)
    horaire_vendredi = Column(MEDIUMTEXT)
    horaire_samedi = Column(MEDIUMTEXT)
    horaire_dimanche = Column(MEDIUMTEXT)
    score = Column(FLOAT)
    avis = Column(Integer)
    siret = Column(VARCHAR(100))
    code_naf = Column(MEDIUMTEXT)
    effectif_etablissement = Column(MEDIUMTEXT)
    typologie = Column(MEDIUMTEXT)
    siren = Column(Integer)
    adresse_siege = Column(MEDIUMTEXT)
    forme_juridique = Column(MEDIUMTEXT)
    date_creation_entreprise = Column(DateTime)
    capital_social = Column(MEDIUMTEXT)
    effectif_entreprise = Column(MEDIUMTEXT)
    tva_intracommunautaire = Column(MEDIUMTEXT)
    autres_denominations = Column(MEDIUMTEXT)
    date_chiffres_cles = Column(DateTime)
    ca = Column(MEDIUMTEXT)
    ebe = Column(MEDIUMTEXT)
    resultat_net = Column(MEDIUMTEXT)
    chambres = Column(Integer)
    capacites = Column(MEDIUMTEXT)
    guides = Column(MEDIUMTEXT)
    reseaux = Column(MEDIUMTEXT)
    couverts = Column(Integer)
    nom_du_chef = Column(MEDIUMTEXT)
    principaux_dirigeants = Column(MEDIUMTEXT)

    keyword = Column(MEDIUMTEXT)
    location = Column(MEDIUMTEXT)
    page = Column(Integer)
    scraping_time = Column(DateTime)

    def __init__(self, source=None):
        if source is not None:
            self.__dict__.update(source.__dict__)


class Monitoring(Base):
    __tablename__ = 'monitoring'

    id = Column(Integer, primary_key=True, autoincrement=True)
    keyword = Column(MEDIUMTEXT)
    location = Column(MEDIUMTEXT)
    total_pages = Column(Integer)
    last_page_scraped = Column(Integer)
    scraping_time = Column(DateTime)
    is_done = Column(BOOLEAN, default=0)

def create_all(engine):
    print("creating databases")
    Base.metadata.create_all(engine)
