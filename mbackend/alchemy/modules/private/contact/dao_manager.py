from mbackend.alchemy.scoped_dao.dao_manager import DaoManager
from mbackend.alchemy.scoped_dao.dao_manager import DaoManager
from mbackend.alchemy.scoped_dao.dao_manager import DaoManager
from .dao_factory import DaoFactory


class DaoManager(DaoManager):

    def __init__(self, table_name, engine_config=None):
        super(DaoManager, self).__init__()
        self.daoFactory = DaoFactory(table_name, engine_config)

    def get_shared_session(self):
        return self.daoFactory.get_shared_session()

    def select_no_scraped_people(self, limit):
        return self.daoFactory.select_no_scraped_people(limit)

    def insert_contact(self, obj, ref_id):
        return self.daoFactory.insert_contact(obj, ref_id)

    def count_contact(self, obj):
        return self.daoFactory.count_contact(obj)

    def update_people(self, ref_id):
        return self.daoFactory.update_people(ref_id)
