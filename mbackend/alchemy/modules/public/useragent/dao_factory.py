from mbackend.alchemy.scoped_dao.dao_factory import DaoFactory

from .tables import create_all

from mbackend.tools.db_tools.decorators import dbconnect, dbconnect_noexpire, dbconnect_noclosing, dbconnect_noclosingandsession, dbconnect_sharedsession
from pytz import timezone
import datetime
from sqlalchemy import and_, or_

from sqlalchemy.orm import scoped_session


class DaoFactory(DaoFactory):

    def __init__(self, database_name, engine_config=None):
        self.timezone = timezone('Europe/Paris')
        super(DaoFactory, self).__init__(database_name, engine_config)
        create_all(self.engine)

    def get_shared_session(self):
        ScopedSession = scoped_session(self.session_factory)
        session = ScopedSession()
        return session, ScopedSession

    @dbconnect_noclosingandsession
    def select_by_id(self, session, alchemy_object, id):
        return session.query(alchemy_object).filter(alchemy_object.id == id).one()

    @dbconnect
    def count_by_keyword(self, session, alchemy_object, keyword):
        return session.query(alchemy_object).filter(alchemy_object.keyword == keyword).count()

    @dbconnect
    def select_by__id(self, session, alchemy_object, _id):
        obj = session.query(alchemy_object).filter(alchemy_object.id == _id).one()
        return obj.keyword

    @dbconnect
    def update_by__id(self, session, alchemy_object, filled_object, keyword):
        obj = session.query(alchemy_object).filter(alchemy_object.id == filled_object.id).one()
        obj.keyword = keyword
        obj.name = filled_object.name
        obj.website = filled_object.website
        obj.category = filled_object.category
        obj.address = filled_object.address
        obj.phone = filled_object.phone
        obj.reviews = filled_object.reviews
        obj.score = filled_object.score
        obj.is_done = 1
        obj.scraping_time = datetime.datetime.now()
        session.commit()

    @dbconnect_noclosingandsession
    def select_not_done(self, session, alchemy_object):
        return session.query(alchemy_object).filter(alchemy_object.is_done == 0).all()
