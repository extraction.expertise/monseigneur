from monseigneur.core.browser import PagesBrowser, URL, StatesMixin
from monseigneur.core.browser.filters.base import ItemNotFound
from monseigneur.core.browser.exceptions import ClientError, ServerError

from monseigneur.core.exceptions import BrowserBanned
from requests.exceptions import ProxyError, ChunkedEncodingError

from monseigneur.core.capabilities.base import NotAvailable
from monseigneur.core.tools.decorators import retry

from requests.exceptions import ConnectionError
from .pages import HousingListPage, HousingDetailsPage


class PapImmoBrowser(PagesBrowser, StatesMixin):

    TIMEOUT = 10
    BASEURL = 'https://www.pap.fr/'
    VERIFY = False

    buying_annonces = URL('/annonce/vente-immobiliere-paris-75-g439g442g456',
                          '/annonce/(.*)', HousingListPage)
    annonce_details = URL('/annonces/(.*)', HousingDetailsPage)

    def __init__(self, *args, **kwargs):
        super(PapImmoBrowser, self).__init__(*args, **kwargs)

    def locate_browser(self, state):
        pass

    def iter_buying_annonces(self, url):
        self.location(url)
        self.last_main_page = self.page
        return self.page.iter_annonces()

    def fill_annonce(self, annonce):
        self.location(annonce.url)
        return self.page.fill_annonce(annonce)

    def get_nextpage(self):
        return self.last_main_page.get_nextpage()
