from monseigneur.core.browser.pages import JsonPage
from monseigneur.core.browser.elements import method, DictElement, ItemElement
from monseigneur.modules.public.lequipe.alchemy.tables import Article
from monseigneur.core.browser.filters.json import Dict
from monseigneur.core.browser.filters.standard import CleanDecimal, CleanDate


class ApiPage(JsonPage):

    def build_doc(self, text):
        return JsonPage.build_doc(self, text)

    @method
    class iter_articles(DictElement):

        def find_elements(self):
            for el in self.page.doc['content']['feed']['items']:
                if 'content' in el.keys():
                    yield el
                continue

        class get_article(ItemElement):
            klass = Article

            def obj_internal_id(self):
                return int(Dict('content/id')(self.el))

            """def obj_internal_id(self):
                return int(self.el['content']['id'])"""

            def obj_publication_time(self):
                raw_time = Dict('content/publication_date')(self.el)
                assert raw_time
                return CleanDate().filter(raw_time)

            """def obj_publication_time(self):
                raw_time = Dict('content/publication_date')(self.el)
                raw_time = raw_time.split('+')[0]
                raw_time = raw_time.replace('T', ' ')
                return datetime.strptime(raw_time, '%Y-%m%d %H:%M:%S')"""



