# -*- coding: utf-8 -*-

# Copyright(C) 2018 Simon Rochwerg

from monseigneur.core.tools.backend import Module

from .browser import UserAgentBrowser

__all__ = ['UserAgentModule']


class UserAgentModule(Module):
    NAME = 'useragent'
    MAINTAINER = u'Sasha & Simon'
    EMAIL = '{first}.{last}@lobstr.io'
    VERSION = '1.4'
    DESCRIPTION = u'Google 4all'
    LICENSE = 'AGPL'
    BROWSER = UserAgentBrowser

    def iter_user_agents(self):
        return self.browser.iter_user_agents()
