from mbackend.core.fetcher import Fetcher
from mbackend.core.application import Application
from alchemy.dao_manager import DaoManager
from alchemy.tables import Housing

from mbackend.tools.proxies import proxies
import time


class LeboncoinBackend(Application):

    APPNAME = "Application Leboncoin"
    VERSION = '1.0'
    COPYRIGHT = 'Copyright(C) 2012-YEAR LOBSTR'
    DESCRIPTION = "Scraping Backend for Leboncoin"
    SHORT_DESCRIPTION = "Step-by-step Example of Leboncoin Scraping"

    def __init__(self):
        super(LeboncoinBackend, self).__init__(self.APPNAME)
        self.setup_logging()
        self.fetcher = Fetcher(is_public=True)
        self.proxies = proxies()
        self.module = self.fetcher.build_backend("leboncoin", params={})

        # Put your proxy
        # self.backend_module.browser.PROXIES = self.proxies.forge_proxy_dictionary("your proxy here")

        # SQLAlchemy to integrate
        self.dao = DaoManager("leboncoin")
        self.session, self.scoped_session = self.dao.get_shared_session()

    def main(self):
        for page in range(1, 2, 1):
            housings = self.module.iter_housing(category=9, location="Cassis_13260", real_estate_type=1, page=page)
            for housing in housings:
                if not self.session.query(Housing).filter(Housing.housing_id == housing.housing_id).count():
                    print(housing.__dict__)
                    self.session.add(housing)

                self.session.commit()

            time.sleep(60)



if __name__ == '__main__':
    my = LeboncoinBackend()
    my.main()
