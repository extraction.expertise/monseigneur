# -*- coding: utf-8 -*-

# Copyright(C) 2018 Sasha Bouloudnine

from monseigneur.core.browser import PagesBrowser, URL
from .pages import ListPage

__all__ = ['LeboncoinBrowser']


class LeboncoinBrowser(PagesBrowser):

    BASEURL = 'https://www.leboncoin.fr/'

    list_page = URL("/recherche/\?category=(?P<category>\d+)&locations=(?P<location>.+)&real_estate_type=(?P<real_estate_type>\d+)&page=(?P<page>\d+)", ListPage)

    def __init__(self, *args, **kwargs):
        super(LeboncoinBrowser, self).__init__(*args, **kwargs)

    def iter_housing(self, category, location, real_estate_type, page):
        self.list_page.go(category=category, location=location, real_estate_type=real_estate_type, page=page)
        assert self.list_page.is_here()
        return self.page.iter_housing()
