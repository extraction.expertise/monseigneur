# -*- coding: utf-8 -*-

# Copyright(C) 2018 Sasha Bouloudnine

from monseigneur.core.tools.backend import Module
from .browser import LeboncoinBrowser

__all__ = ['LeboncoinModule']


class LeboncoinModule(Module):
    NAME = 'Leboncoin'
    MAINTAINER = u'Sasha & Simon'
    EMAIL = '{first}.{last}@lobstr.io'
    VERSION = '1.4'
    DESCRIPTION = u'Leboncoin 4all'
    LICENSE = 'AGPL'
    BROWSER = LeboncoinBrowser

    def iter_housing(self, category, location, real_estate_type, page):
        return self.browser.iter_housing(category, location, real_estate_type, page)
