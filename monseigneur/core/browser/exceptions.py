# -*- coding: utf-8 -*-

from requests.exceptions import HTTPError
from monseigneur.core.exceptions import BrowserHTTPError, BrowserHTTPNotFound


class HTTPNotFound(HTTPError, BrowserHTTPNotFound):
    pass


class ClientError(HTTPError, BrowserHTTPError):
    pass


class ServerError(HTTPError, BrowserHTTPError):
    pass


class LoggedOut(Exception):
    pass
